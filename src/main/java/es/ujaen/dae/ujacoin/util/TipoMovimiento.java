/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoin.util;

/**
 * Enum para el tipo de movimiento
 *
 * @author Adrian
 */
public enum TipoMovimiento {
    TMIngreso,
    TMReintegro,
    TMTransferenciaEmitida,
    TMTrasnferenciaRecibida
}
