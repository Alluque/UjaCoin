/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoin.interfaces;

import es.ujaen.dae.ujacoin.entidades.Cliente;
import es.ujaen.dae.ujacoin.entidades.Tarjeta;
import es.ujaen.dae.ujacoin.entidades.Cuenta;
import java.util.ArrayList;

/**
 * Interfaz para las operaciones publicas del servidor
 *
 * @author Adrian
 */
public interface ServicioUjaBank {

    /**
     * Da de alta a un nuevo cliente y le asocia una cuenta
     *
     * @param cliente cliente a crear
     */
    public void altaCliente(Cliente cliente);

    /**
     * Comprueba las credenciales del cliente
     *
     * @param dni Dni del cliente
     * @param clave Contraseña
     * @return Cliente si las credenciales son válidas, Null si no
     */
    public Cliente loginCliente(String dni, String clave);
    
    /**
     * Devuelve un cliente por DNI usado por REST
     *
     * @param dni Dni del cliente
     * @return Cliente si las credenciales son válidas, Null si no
     */
    public Cliente loginCliente(String dni);

    /**
     * Lista las tarjetas asociadas a un cliente
     *
     * @param cliente cliente
     * @return Lista de las tarjetas
     */
    public ArrayList<Tarjeta> listaTarjetas(Cliente cliente);

    /**
     * Listas las cuentas asociadas a un cliente
     *
     * @param cliente Dni del cliente
     * @return Lista de cuentas
     */
    public ArrayList<Cuenta> listaCuentas(Cliente cliente);

    /**
     * Devuelve una cuenta localizando su número
     *
     * @param numero Identificador de la cuenta
     * @return
     */
    public Cuenta verCuenta(String numero);

    /**
     * Genera un ingreso en la cuenta del cliente desde una tarjeta
     *
     * @param cuenta Cuenta destino
     * @param tarjeta Tarjeta origen
     * @param importe Importe de la operación
     */
    public void ingreso(Cuenta cuenta, Tarjeta tarjeta, float importe);

    /**
     * Genera un reintegro desde una cuenta del cliente hacia una tarjeta
     *
     * @param cuenta Cuenta origen
     * @param tarjeta Tarjeta destino
     * @param importe Importe de la operación
     */
    public void reintegro(Cuenta cuenta, Tarjeta tarjeta, float importe);

    /**
     * Genera una transferencia entre cuentas
     *
     * @param cuentaOrigen Cuenta origen
     * @param cuentaDestino Cuenta destino
     * @param importe Importe de la operación
     */
    public void transferencia(Cuenta cuentaOrigen, Cuenta cuentaDestino, float importe);

    /**
     * Genera una cuenta nueva para el cliente y la devuelve
     *
     * @param cliente Cliente al que se le asignara la nueva cuenta
     * @return la cuenta recién creada
     */
    public Cuenta añadirCuenta(Cliente cliente);

    /**
     * Genera una tarjeta nueva para el cliente y la devuelve
     *
     * @param cliente Cliente al que se le asignara la nueva cuenta
     * @param tarjeta tarjeta para vincular al clente
     */
    public void añadirTarjeta(Cliente cliente, Tarjeta tarjeta);

    /**
     * Borra una cuenta con saldo = 0
     *
     * @param cuenta cuenta a borrar
     */
    public void borrarCuenta(Cuenta cuenta);

    /**
     * Borra una tarjeta
     *
     * @param identificador tarjeta a borrar
     */
    public void borrarTarjeta(int identificador);

    /**
     * Actualiza los datos personales de un cliente
     * @param clienteNuevo Cliente para actualizar
     * @return El cliente actualizado
     */
    public Cliente actualizarCliente(Cliente clienteNuevo);
    
}
