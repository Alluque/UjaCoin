/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoin.entidades;

import es.ujaen.dae.ujacoin.util.TipoMovimiento;
import es.ujaen.dae.ujacoin.excepciones.CuentaNoValida;
import es.ujaen.dae.ujacoin.excepciones.TarjetaNoValida;
import es.ujaen.dae.ujacoin.excepciones.TipoMovimientoInvalido;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * Movimientos asociados a las cuentas
 *
 * @author Adrian
 */
@Entity
public class Movimiento {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int identificador;

    /**
     * Tipo de movimiento
     */
    private TipoMovimiento tipo;

    /**
     * Importe del movimiento
     */
    private float importe;

    /**
     * Fecha y hora del movimiento
     */
    private LocalDateTime fechaHora;

    /**
     * Cuenta destino/origen
     */
    @OneToOne//Eager por defecto
    @JoinColumn(name = "cuentaOD")
    private Cuenta cuenta;

    /**
     * Tarjeta destino/origen
     */
    @OneToOne//Eager por defecto
    @JoinColumn(name = "tarjetaOD")
    private Tarjeta tarjeta;

    public Movimiento() {
    }

    public Movimiento(TipoMovimiento tipo, float importe, LocalDateTime fechaHora, Tarjeta tarjeta) {

        if (!(tipo == TipoMovimiento.TMIngreso || tipo == TipoMovimiento.TMReintegro)) {
            throw new TipoMovimientoInvalido();
        }

        if (!Tarjeta.checkNumeroTarjeta(tarjeta.getNumero())) {
            throw new TarjetaNoValida();
        }

        this.tipo = tipo;
        this.importe = importe;
        this.fechaHora = fechaHora;
        this.tarjeta = tarjeta;
    }

    public Movimiento(TipoMovimiento tipo, float importe, LocalDateTime fechaHora, Cuenta cuenta) {

        if (!(tipo == TipoMovimiento.TMTransferenciaEmitida || tipo == TipoMovimiento.TMTrasnferenciaRecibida)) {
            throw new TipoMovimientoInvalido();
        }

        if (!Cuenta.checkNumeroCuenta(cuenta.getNumero())) {
            throw new CuentaNoValida();
        }

        this.tipo = tipo;
        this.importe = importe;
        this.fechaHora = fechaHora;
        this.cuenta = cuenta;
    }

    public TipoMovimiento getTipo() {
        return tipo;
    }

    public float getImporte() {
        return importe;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public Tarjeta getTarjeta() {
        return tarjeta;
    }

    public int getIdentificador() {
        return identificador;
    }

    public LocalDateTime getFechaHora() {
        return fechaHora;
    }

}
