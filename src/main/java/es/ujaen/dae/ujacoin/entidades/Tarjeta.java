/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoin.entidades;

import es.ujaen.dae.ujacoin.excepciones.TarjetaNoValida;
import java.time.LocalDate;
import java.util.Random;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Tarjetas de crédito asociadas a clientes
 *
 * @author Adrian
 */
@Entity
public class Tarjeta {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int Identificador;

    /**
     * Número de la tarjeta
     */
    private String numero;
    /**
     * Titular de la tarjeta
     */
    private String titular;
    /**
     * Código de seguridad de la tarjeta
     */
    private String cvc;
    /**
     * Fecha de caducidad de la tarjeta
     */
    private LocalDate fechaCaducidad;

    public Tarjeta() {
    }

    private static Random rand = new Random();

    public Tarjeta(String numero, String titular, String cvc, LocalDate fechaCaducidad) {

        if (!checkNumeroTarjeta(numero)) {
            throw new TarjetaNoValida();
        }

        this.numero = numero;
        this.titular = titular;
        this.cvc = cvc;
        this.fechaCaducidad = fechaCaducidad;

    }

    public String getNumero() {
        return numero;
    }

    public String getTitular() {
        return titular;
    }

    public String getCvc() {
        return cvc;
    }

    public LocalDate getFechaCaducidad() {
        return fechaCaducidad;
    }

    public int getIdentificador() {
        return Identificador;
    }

    /**
     * Comprueba que el número de tarjeta es válido, 8 dígitos numéricos, la
     * suma de los dígitos tiene que ser multiplo de 4.
     *
     * @param numero Numero de tarjeta a comprobar
     * @return True si el número es válido, false en otro caso
     */
    public static boolean checkNumeroTarjeta(String numero) {

        if (numero.matches("[0-9]+") && numero.length() == 8) {

            int suma = 0;

            for (int i = 0; i < numero.length(); i++) {

                suma += Integer.parseInt(numero.substring(i, i + 1));

            }

            if (suma % 4 == 0) {
                return true;
            }
        }

        return false;

    }

    public static String generadorTarjetasNuevas() {

        boolean numeroValido = false;
        String numeroCadena;

        do {
            int numero = rand.nextInt(99999999);
            numeroCadena = String.valueOf(numero);

            for (int i = numeroCadena.length(); i < 8; i++) {
                numeroCadena = "0".concat(numeroCadena);
            }

            int codigo = 0;

            for (int i = 0; i < 8; i++) {
                codigo += Integer.parseInt(numeroCadena.substring(i, i + 1));
            }

            codigo = codigo % 4;

            if ((numero % 10) > 5) {
                numero -= codigo;
            } else {
                numero += 4 - codigo;
            }

            numeroCadena = String.valueOf(numero);

            for (int i = numeroCadena.length(); i < 8; i++) {
                numeroCadena = "0".concat(numeroCadena);
            }

        } while (!numeroValido);

        return numeroCadena;

    }

}
