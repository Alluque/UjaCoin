/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoin.entidades;


import es.ujaen.dae.ujacoin.excepciones.CuentaNoValida;
import es.ujaen.dae.ujacoin.excepciones.SaldoInsuficiente;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Cuenta de UjaCoins
 *
 * @author Adrian
 */
@Entity
public class Cuenta {

    /**
     * Número de identificación de la cuenta
     */
    @Id
    private String numero;

    /**
     * Titular de la cuenta
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "titularCuenta")
    private Cliente titular;

    /**
     * Saldo de la cuenta
     */
    private float saldo;
    /**
     * Movimientos de la cuenta
     */
    @OneToMany
    private List<Movimiento> movimientos;

    public Cuenta() {
    }

    public Cuenta(String numero, Cliente titular) {

        if (!checkNumeroCuenta(numero)) {
            throw new CuentaNoValida();
        }

        this.numero = numero;
        this.saldo = 0;
        this.titular = titular;
        this.movimientos = new ArrayList<>();
    }

    public String getNumero() {
        return numero;
    }

    public float getSaldo() {
        return saldo;
    }

    public Cliente getTitular() {
        return titular;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    public List<Movimiento> getMovimientos() {
        return movimientos;
    }
    
    /**
     * Modifica el saldo, lanza excepción si el saldo resultante es negativo.
     * @param importe cambio
     */
    public void modificarSaldo(float importe){
    
                
        if ((saldo + importe) < 0) {
            throw new SaldoInsuficiente();
        }
        
        saldo += importe;
    
    }
    
    /**
     * Comprueba que el número de cuenta es válido, 12 dígitos numéricos, los
     * dos últimos dígitos son la suma de los anteriores modulo 100.
     *
     * @param numero Numero de cuenta a comprobar
     * @return True si el número es válido, false en otro caso
     */
    public static boolean checkNumeroCuenta(String numero) {

        if (numero.matches("[0-9]+") && numero.length() == 10) {

            int suma = 0;
            int codigoSeguridad = Integer.parseInt(numero.substring(8, 10));

            for (int i = 0; i < 8; i++) {

                suma += Integer.parseInt(numero.substring(i, i + 1));

            }

            if (suma % 100 == codigoSeguridad) {
                return true;
            }
        }

        return false;

    }
    
}
