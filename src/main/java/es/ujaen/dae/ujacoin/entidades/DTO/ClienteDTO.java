/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoin.entidades.DTO;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.springframework.hateoas.Link;

/**
 * Clase cliente de UjaBank
 *
 * @author Adrian
 */
public class ClienteDTO {

    private String dni;
    private String nombre;
    private LocalDate fechaNacimiento;
    private String direccion;
    private String telefono;
    private String email;
    private String clave;
    
    private List<Link> cuentasAsociadas;
    private List<Link> tarjetasAsociadas;
    
    public ClienteDTO() {
    }

    public ClienteDTO(String dni, String nombre, LocalDate fechaNacimiento, String direccion, String telefono, String email) {
        this.dni = dni;
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
        this.direccion = direccion;
        this.telefono = telefono;
        this.email = email;
        this.cuentasAsociadas = new ArrayList<>();
        this.tarjetasAsociadas = new ArrayList<>();
    }

    public String getDni() {
        return dni;
    }

    public String getNombre() {
        return nombre;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getEmail() {
        return email;
    }

    public String getClave() {
        return clave;
    }

    public List<Link> getCuentasAsociadas() {
        return cuentasAsociadas;
    }

    public List<Link> getTarjetasAsociadas() {
        return tarjetasAsociadas;
    }

    public void añadirCuenta(Link cuenta){
        cuentasAsociadas.add(cuenta);
    }
    
    public void añadirTarjeta(Link tarjeta){
        cuentasAsociadas.add(tarjeta);
    }

}
