/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoin.entidades.DTO;


import es.ujaen.dae.ujacoin.entidades.Cliente;
import es.ujaen.dae.ujacoin.excepciones.CuentaNoValida;
import org.springframework.hateoas.Link;

/**
 * Cuenta de UjaCoins
 *
 * @author Adrian
 */
public class CuentaDTO {

    private String numero;
    private float saldo;

    public CuentaDTO() {
    }

    public CuentaDTO(String numero, float saldo) {

        if (!checkNumeroCuenta(numero)) {
            throw new CuentaNoValida();
        }

        this.numero = numero;
        this.saldo = saldo;
        
    }

    public String getNumero() {
        return numero;
    }

    public float getSaldo() {
        return saldo;
    }

    
    /**
     * Comprueba que el número de cuenta es válido, 12 dígitos numéricos, los
     * dos últimos dígitos son la suma de los anteriores modulo 100.
     *
     * @param numero Numero de cuenta a comprobar
     * @return True si el número es válido, false en otro caso
     */
    public static boolean checkNumeroCuenta(String numero) {

        if (numero.matches("[0-9]+") && numero.length() == 10) {

            int suma = 0;
            int codigoSeguridad = Integer.parseInt(numero.substring(8, 10));

            for (int i = 0; i < 8; i++) {

                suma += Integer.parseInt(numero.substring(i, i + 1));

            }

            if (suma % 100 == codigoSeguridad) {
                return true;
            }
        }

        return false;

    }

}
