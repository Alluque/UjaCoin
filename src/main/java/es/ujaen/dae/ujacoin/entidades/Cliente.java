/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoin.entidades;

import es.ujaen.dae.ujacoin.excepciones.TarjetaYaVinculada;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;

/**
 * Clase cliente de UjaBank
 *
 * @author Adrian
 */
@Entity
public class Cliente {

    /**
     * Dni del cliente
     */
    @Id
    private String dni;
    /**
     * Nombre del cliente
     */
    private String nombre;
    /**
     * Fecha de nacimiento del cliente
     */
    private LocalDate fechaNacimiento;
    /**
     * Dirección del cliente
     */
    private String direccion;
    /**
     * Teléfono del cliente
     */
    private String telefono;
    /**
     * Email del cliente
     */
    private String email;
    /**
     * Contraseña del cliente
     */
    private String clave;
    /**
     * Cuentas de las que es titular
     */
    @OneToMany(mappedBy = "titular", fetch = FetchType.EAGER)
    @MapKey(name = "numero")
    private Map<String, Cuenta> cuentasAsociadas;
    /**
     * Cuentas de las que es titular
     */
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn
    @MapKey(name = "numero")
    private Map<String, Tarjeta> tarjetasAsociadas;

    public Cliente() {
    }

    public Cliente(String dni, String nombre, LocalDate fechaNacimiento, String direccion, String telefono, String email, String clave) {
        this.dni = dni;
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
        this.direccion = direccion;
        this.telefono = telefono;
        this.email = email;
        this.clave = clave;
        this.cuentasAsociadas = new HashMap<>();
        this.tarjetasAsociadas = new HashMap<>();
    }

    public String getDni() {
        return dni;
    }

    public String getNombre() {
        return nombre;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getEmail() {
        return email;
    }

    public String getClave() {
        return clave;
    }

    public Map<String, Cuenta> getCuentasAsociadas() {
        return cuentasAsociadas;
    }

    public Map<String, Tarjeta> getTarjetasAsociadas() {
        return tarjetasAsociadas;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Comprueba si la clave introducida coincide con la guardada
     *
     * @param clave
     * @return
     */
    public boolean claveVálida(String clave) {

        return this.clave.compareTo(clave) == 0;

    }

    /**
     * Añade una cuenta al cliente
     *
     * @param cuenta
     */
    public void asignarCuenta(Cuenta cuenta) {
        cuentasAsociadas.put(cuenta.getNumero(), cuenta);
    }

    /**
     * Desenlaza la cuenta del usuario
     *
     * @param cuenta cuenta a remover
     */
    public void desenlazarCuenta(Cuenta cuenta) {
        cuentasAsociadas.remove(cuenta.getNumero());
    }

    /**
     * Añade una tarjeta al cliente
     *
     * @param tarjeta
     */
    public void añadirTarjeta(Tarjeta tarjeta) {

        if (tarjetasAsociadas.get(tarjeta.getNumero()) != null
                && tarjetasAsociadas.get(tarjeta.getNumero()).getCvc().equals(tarjeta.getCvc())) {

            throw new TarjetaYaVinculada();

        }

        tarjetasAsociadas.put(tarjeta.getNumero(), tarjeta);
    }

}
