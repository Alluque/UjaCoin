/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoin.entidades.DTO;

import es.ujaen.dae.ujacoin.util.TipoMovimiento;
import java.time.LocalDateTime;

/**
 * Movimientos asociados a las cuentas
 *
 * @author Adrian
 */
public class MovimientoDTO {

    private TipoMovimiento tipo;
    private float importe;
    private LocalDateTime fechaHora;

    private String tarjetaCuenta;

    public MovimientoDTO() {
    }
    
    public MovimientoDTO(TipoMovimiento tipo, float importe, LocalDateTime fechaHora, String tarjetaCuenta) {
        this.tipo = tipo;
        this.importe = importe;
        this.fechaHora = fechaHora;
    }

    public TipoMovimiento getTipo() {
        return tipo;
    }

    public float getImporte() {
        return importe;
    }

    public String getTarjetaCuenta() {
        return tarjetaCuenta;
    }

    public LocalDateTime getFechaHora() {
        return fechaHora;
    }

}
