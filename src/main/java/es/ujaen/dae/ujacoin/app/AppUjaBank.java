/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoin.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

/**
 *
 * @author Adrian
 */
/**
 * Clase principal de arranque de la aplicación basada en SpringBoot
 */
@SpringBootApplication(scanBasePackages = "es.ujaen.dae.ujacoin.beans")
@EntityScan(basePackages = "es.ujaen.dae.ujacoin.entidades")
public class AppUjaBank {

    public static void main(String[] args) {
        // No necesitamos el contexto de aplicación aquí,
        // así que puede usarse un arranque simplificado
        SpringApplication.run(AppUjaBank.class, args);
    }

}
