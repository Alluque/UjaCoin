/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoin.beans;

import es.ujaen.dae.ujacoin.entidades.Cliente;
import es.ujaen.dae.ujacoin.entidades.Cuenta;
import es.ujaen.dae.ujacoin.entidades.Tarjeta;
import es.ujaen.dae.ujacoin.entidades.DTO.ClienteDTO;
import es.ujaen.dae.ujacoin.entidades.DTO.CuentaDTO;
import es.ujaen.dae.ujacoin.entidades.DTO.TarjetaDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ujacoin")
public class ServicioRestAPI {

    @Autowired
    ServicioUjaBankImpl ujaBank;

    @Autowired
    Mapeador mapper;

    ////////TESTING
    @GetMapping("/enteros/{entero}")
    public ResponseEntity<Integer> testEntero(@PathVariable int entero) {

        if (entero < 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>(entero - 1, HttpStatus.OK);
        }
    }
    
    @GetMapping("/clientes/{dni}")
    public ResponseEntity<ClienteDTO> loginCliente(@PathVariable String dni) {

        if (dni != null && "".equals(dni)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        boolean admin = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
                .stream().anyMatch(r -> r.getAuthority().equals("ROLE_ADMIN"));

        if (!admin && !username.equals(dni)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Cliente cliente = ujaBank.loginCliente(dni);

        if (cliente == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(mapper.aClienteDTO(cliente), HttpStatus.OK);

    }
    @PutMapping("/clientes/{dni}")
    public ResponseEntity<ClienteDTO> actualizarDetalleCliente(@PathVariable String dni,@RequestBody ClienteDTO cliente) {

        if (dni != null && "".equals(dni)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        boolean admin = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
                .stream().anyMatch(r -> r.getAuthority().equals("ROLE_ADMIN"));

        if (!admin && !username.equals(dni)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if(!username.equals(cliente.getDni())){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        
        //Comprobar datos entrantes
        
        ujaBank.actualizarCliente(mapper.deClienteDTO(cliente));

        return new ResponseEntity<>(HttpStatus.OK);

    }
    
    @GetMapping("/clientes/{dni}/cuentas/{num}")
    public ResponseEntity<CuentaDTO> detalleCuenta(@PathVariable String dni, @PathVariable String num) {

        if (dni != null && "".equals(dni) && num != null && "".equals(num)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        boolean admin = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
                .stream().anyMatch(r -> r.getAuthority().equals("ROLE_ADMIN"));

        if (!admin && !username.equals(dni)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Cliente cliente = ujaBank.loginCliente(dni);

        if (cliente == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        Cuenta cuenta = null;
        List<Cuenta> listaCuentas = ujaBank.listaCuentas(cliente);

        for (Cuenta c : listaCuentas) {
            if (c.getNumero().equals(num)) {
                cuenta = c;
                break;
            }
        }

        if (cuenta == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(mapper.aCuentaDTO(cuenta), HttpStatus.OK);

    }

    @GetMapping("/clientes/{dni}/tarjetas/{num}")
    public ResponseEntity<TarjetaDTO> detalleTarjeta(@PathVariable String dni, @PathVariable int num) {

        if (dni != null && "".equals(dni) && num > 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        boolean admin = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
                .stream().anyMatch(r -> r.getAuthority().equals("ROLE_ADMIN"));

        if (!admin && !username.equals(dni)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Cliente cliente = ujaBank.loginCliente(dni);

        if (cliente == null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        Tarjeta tarjeta = null;
        List<Tarjeta> listaTarjetas = ujaBank.listaTarjetas(cliente);

        for (Tarjeta t : listaTarjetas) {
            if (t.getIdentificador() == num) {
                tarjeta = t;
                break;
            }
        }

        if (tarjeta == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(mapper.aTarjetaDTO(tarjeta), HttpStatus.OK);

    }

}
