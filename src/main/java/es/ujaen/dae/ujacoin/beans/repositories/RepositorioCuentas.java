/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoin.beans.repositories;

import es.ujaen.dae.ujacoin.entidades.Cliente;
import es.ujaen.dae.ujacoin.entidades.Cuenta;
import es.ujaen.dae.ujacoin.entidades.Movimiento;
import es.ujaen.dae.ujacoin.entidades.Tarjeta;
import es.ujaen.dae.ujacoin.excepciones.TarjetaNoValida;
import es.ujaen.dae.ujacoin.util.TipoMovimiento;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author adria
 */
@Repository
@Transactional
public class RepositorioCuentas {

    @PersistenceContext
    EntityManager em;

    public Cuenta buscar(String numero) {
        return em.find(Cuenta.class, numero);
    }

    public void insertar(Cuenta cuenta) {
        em.persist(cuenta);
    }

    public void actualizar(Cuenta cuenta) {
        em.merge(cuenta);
    }

    public void eliminar(Cuenta cuenta) {
        em.remove(em.merge(cuenta));
    }
    
    public void transferencia(Cuenta cuentaOrigen, Cuenta cuentaDestino, float importe){
    
        Cuenta cuentaOrigenEnlazada = em.find(Cuenta.class, cuentaOrigen.getNumero(), LockModeType.PESSIMISTIC_WRITE);
        Cuenta cuentaDestinoEnlazada = em.find(Cuenta.class, cuentaDestino.getNumero(), LockModeType.PESSIMISTIC_WRITE);
     
        cuentaOrigenEnlazada.modificarSaldo(-importe);
        cuentaDestinoEnlazada.modificarSaldo(importe);
                
        Movimiento movOrigen = new Movimiento(TipoMovimiento.TMTransferenciaEmitida, importe, LocalDateTime.now(), cuentaDestino);
        Movimiento movDestino = new Movimiento(TipoMovimiento.TMTrasnferenciaRecibida, importe, LocalDateTime.now(), cuentaOrigen);
               
        em.persist(movOrigen);
        em.persist(movDestino);
        
        cuentaOrigenEnlazada.getMovimientos().add(movOrigen);
        cuentaDestinoEnlazada.getMovimientos().add(movDestino);
        
    }
    
    public void ingreso(Tarjeta tarjetaOrigen, Cuenta cuentaDestino, float importe){
    
        Cuenta cuentaDestinoEnlazada = em.find(Cuenta.class, cuentaDestino.getNumero(), LockModeType.PESSIMISTIC_WRITE);
     
        if(!cuentaDestinoEnlazada.getTitular().getTarjetasAsociadas().containsKey(tarjetaOrigen.getNumero()))    
            throw new TarjetaNoValida();
        
        cuentaDestinoEnlazada.modificarSaldo(importe);
                
        Movimiento movimiento = new Movimiento(TipoMovimiento.TMIngreso, importe, LocalDateTime.now(), tarjetaOrigen);
               
        em.persist(movimiento);
        
        cuentaDestinoEnlazada.getMovimientos().add(movimiento);
        
    }
    
    public void reintegro(Cuenta cuentaOrigen, Tarjeta tarjetaDestino, float importe){
    
        Cuenta cuentaOrigenEnlazada = em.find(Cuenta.class, cuentaOrigen.getNumero(), LockModeType.PESSIMISTIC_WRITE);

        if(!cuentaOrigenEnlazada.getTitular().getTarjetasAsociadas().containsKey(tarjetaDestino.getNumero()))    
            throw new TarjetaNoValida();
            
        cuentaOrigenEnlazada.modificarSaldo(-importe);
                
        Movimiento movimiento = new Movimiento(TipoMovimiento.TMReintegro, importe, LocalDateTime.now(), tarjetaDestino);
               
        em.persist(movimiento);
        
        cuentaOrigenEnlazada.getMovimientos().add(movimiento);
        
    }
    
    public List<Cuenta> listarCuentasPorCliente(Cliente cliente) {

        em.merge(cliente);
        return new ArrayList<>(cliente.getCuentasAsociadas().values());

    }
    
    public void añadirMovimiento(Cuenta cuenta, Movimiento movimiento){
    
        Cuenta cuentaEnlazada = em.merge(cuenta);
        
        cuentaEnlazada.getMovimientos().add(movimiento);

    }

}
