/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoin.beans;

import es.ujaen.dae.ujacoin.entidades.Cliente;
import es.ujaen.dae.ujacoin.entidades.Cuenta;
import es.ujaen.dae.ujacoin.entidades.Tarjeta;
import es.ujaen.dae.ujacoin.entidades.DTO.ClienteDTO;
import es.ujaen.dae.ujacoin.entidades.DTO.CuentaDTO;
import es.ujaen.dae.ujacoin.entidades.DTO.TarjetaDTO;
import org.springframework.hateoas.server.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Service;

/**
 *
 * @author adria
 */
@Service
public class Mapeador {

    public ClienteDTO aClienteDTO(Cliente cliente) {

        ClienteDTO clienteDTO = new ClienteDTO(
                cliente.getDni(),
                cliente.getNombre(),
                cliente.getFechaNacimiento(),
                cliente.getDireccion(),
                cliente.getTelefono(),
                cliente.getEmail());

        for (String cuenta : cliente.getCuentasAsociadas().keySet()) {
            clienteDTO.añadirCuenta(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(ServicioRestAPI.class).detalleCuenta(cliente.getDni(), cuenta)).withSelfRel());
        }
                
        for (Tarjeta tarjeta : cliente.getTarjetasAsociadas().values()) {
            clienteDTO.añadirCuenta(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(ServicioRestAPI.class).detalleTarjeta(cliente.getDni(), tarjeta.getIdentificador())).withSelfRel());
        }

        return clienteDTO;

    }

    public Cliente deClienteDTO(ClienteDTO clienteDTO) {
        
        Cliente cliente = new Cliente(clienteDTO.getDni(),
                clienteDTO.getNombre(),
                clienteDTO.getFechaNacimiento(),
                clienteDTO.getDireccion(),
                clienteDTO.getTelefono(),
                clienteDTO.getEmail(),
                clienteDTO.getClave());

        return cliente;

    }
    
    public CuentaDTO aCuentaDTO(Cuenta cuenta){
        return new CuentaDTO(cuenta.getNumero(), cuenta.getSaldo());
    }
    
    public TarjetaDTO aTarjetaDTO (Tarjeta tarjeta){
        return new TarjetaDTO(tarjeta.getNumero(), tarjeta.getTitular(), tarjeta.getCvc(), tarjeta.getFechaCaducidad());
    }  
    
    public Tarjeta deTarjetaDTO (TarjetaDTO tarjetaDTO){
        return new Tarjeta(tarjetaDTO.getNumero(), tarjetaDTO.getTitular(), tarjetaDTO.getCvc(), tarjetaDTO.getFechaCaducidad());
    } 
    
}
