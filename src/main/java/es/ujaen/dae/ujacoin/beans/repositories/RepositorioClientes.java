/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoin.beans.repositories;

import es.ujaen.dae.ujacoin.entidades.Cliente;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author adria
 */
@Repository
@Transactional
public class RepositorioClientes {

    @PersistenceContext
    EntityManager em;

    public Cliente buscar(String clave) {
        return em.find(Cliente.class, clave);
    }

    public void insertar(Cliente cliente) {
        em.persist(cliente);
    }

    public void actualizar(Cliente cliente) {
        em.merge(cliente);
    }

    public void eliminar(Cliente cliente) {
        em.remove(em.merge(cliente));
    }

    public Cliente login(String dni, String clave) {

        Cliente cliente = em.createQuery("Select c from Cliente c where c.dni = :dni and c.clave = :clave", Cliente.class).setParameter("dni", dni).setParameter("clave", clave).getSingleResult();

        return cliente;

    }

}
