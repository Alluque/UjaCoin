/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoin.beans;

import es.ujaen.dae.ujacoin.beans.repositories.RepositorioCuentas;
import es.ujaen.dae.ujacoin.beans.repositories.RepositorioClientes;
import es.ujaen.dae.ujacoin.beans.repositories.RepositorioMovimientos;
import es.ujaen.dae.ujacoin.beans.repositories.RepositorioTarjetas;
import es.ujaen.dae.ujacoin.entidades.Cliente;
import es.ujaen.dae.ujacoin.entidades.Cuenta;
import es.ujaen.dae.ujacoin.entidades.Tarjeta;
import es.ujaen.dae.ujacoin.excepciones.DDBBCaida;
import es.ujaen.dae.ujacoin.excepciones.SaldoRestanteNoCero;
import es.ujaen.dae.ujacoin.excepciones.UsuarioYaExistente;
import es.ujaen.dae.ujacoin.interfaces.ServicioUjaBank;
import java.util.ArrayList;
import java.util.Random;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Clase controlador principal del sistema
 */
@Component
public class ServicioUjaBankImpl implements ServicioUjaBank {

    public static Random rand = new Random();
    
    @PersistenceContext
    EntityManager em;

    @Autowired
    private RepositorioClientes repoClientes;

    @Autowired
    private RepositorioCuentas repoCuentas;

    @Autowired
    private RepositorioTarjetas repoTarjetas;

    @Autowired
    private RepositorioMovimientos repoMovimientos;

    public ServicioUjaBankImpl() {

    }

    @Override
    public void altaCliente(Cliente cliente) {

        Cuenta cuentaNueva = new Cuenta(generadorCuentasNuevas(), cliente);
        cliente.asignarCuenta(cuentaNueva);

        try {
            repoClientes.insertar(cliente);
            repoCuentas.insertar(cuentaNueva);
        } catch (org.hibernate.exception.GenericJDBCException e) {
            throw new UsuarioYaExistente();
        } catch (DataAccessResourceFailureException e) {
            throw new DDBBCaida();
        }

    }

    @Override
    public Cliente loginCliente(String dni, String clave) {

        try {

            return repoClientes.login(dni, clave);

        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }
    
    @Override
    public Cliente loginCliente(String dni) {

        try {

            return repoClientes.buscar(dni);

        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    @Transactional
    public ArrayList<Tarjeta> listaTarjetas(Cliente cliente) {
        Cliente clienteEnlazado = em.merge(cliente);
        return new ArrayList<>(clienteEnlazado.getTarjetasAsociadas().values());
    }

    @Override
    public ArrayList<Cuenta> listaCuentas(Cliente cliente) {
        return (ArrayList<Cuenta>) repoCuentas.listarCuentasPorCliente(cliente);
    }

    @Override
    public Cuenta verCuenta(String numero) {

        return repoCuentas.buscar(numero);

    }

    @Override
    public void ingreso(Cuenta cuenta, Tarjeta tarjeta, float importe) {
        repoCuentas.ingreso(tarjeta, cuenta, importe);
    }

    @Override
    public void reintegro(Cuenta cuenta, Tarjeta tarjeta, float importe) {
        repoCuentas.reintegro(cuenta, tarjeta, importe);
    }

    @Override
    public void transferencia(Cuenta cuentaOrigen, Cuenta cuentaDestino, float importe) {
        repoCuentas.transferencia(cuentaOrigen, cuentaDestino, importe);
    }

    @Override
    @Transactional
    public Cuenta añadirCuenta(Cliente cliente) {

        Cuenta cuentaNueva = new Cuenta(generadorCuentasNuevas(), cliente);
        cliente.asignarCuenta(cuentaNueva);

        repoCuentas.insertar(cuentaNueva);
        repoClientes.actualizar(cliente);

        return cuentaNueva;

    }

    @Override
    @Transactional
    public void añadirTarjeta(Cliente cliente, Tarjeta tarjeta) {
        
        cliente.añadirTarjeta(tarjeta);

        repoTarjetas.insertar(tarjeta);
        repoClientes.actualizar(cliente);

    }

    @Override
    @Transactional
    public void borrarCuenta(Cuenta cuenta) {
        if (cuenta.getSaldo() > 0) {
            throw new SaldoRestanteNoCero();
        }

        //Cuidado es lazy
        cuenta.getTitular().desenlazarCuenta(cuenta);
        repoClientes.actualizar(cuenta.getTitular());
        repoCuentas.eliminar(cuenta);

    }

    @Override
    public void borrarTarjeta(int identificador) {

        Tarjeta tarjeta = repoTarjetas.buscar(identificador);

        if (tarjeta != null) {

            repoTarjetas.eliminar(tarjeta);

        }

    }
    
    //SOLO PARA TESTS
    public void asignarSaldoManual(Cuenta cuenta, float saldo){
    
        cuenta.setSaldo(saldo);
        repoCuentas.actualizar(cuenta);
        
    }
    
    private String generadorCuentasNuevas() {

        boolean numeroValido = false;
        String numeroCadena;

        do {
            int numero = rand.nextInt(99999999);
            numeroCadena = String.valueOf(numero);

            for (int i = numeroCadena.length(); i < 8; i++) {
                numeroCadena = "0".concat(numeroCadena);
            }

            int codigo = 0;

            for (int i = 0; i < 8; i++) {
                codigo += Integer.parseInt(numeroCadena.substring(i, i + 1));
            }

            codigo = codigo % 100;

            if (codigo < 10) {
                numeroCadena = numeroCadena.concat("0").concat(String.valueOf(codigo));
            } else {
                numeroCadena = numeroCadena.concat(String.valueOf(codigo));
            }

            if (repoCuentas.buscar(numeroCadena) == null) {
                numeroValido = true;
            }

        } while (!numeroValido);

        return numeroCadena;

    }

    @Override
    public Cliente actualizarCliente(Cliente clienteNuevo) {
        
        Cliente clienteAntiguo = repoClientes.buscar(clienteNuevo.getDni());
        
        clienteAntiguo.setDireccion(clienteNuevo.getDireccion());
        clienteAntiguo.setEmail(clienteNuevo.getEmail());
        clienteAntiguo.setTelefono(clienteNuevo.getTelefono());
        
        repoClientes.actualizar(clienteAntiguo);
        
        return clienteAntiguo;
        
    }

}
