/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoin.beans.repositories;

import es.ujaen.dae.ujacoin.entidades.Cliente;
import es.ujaen.dae.ujacoin.entidades.Tarjeta;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author adria
 */
@Repository
@Transactional
public class RepositorioTarjetas {

    @PersistenceContext
    EntityManager em;

    public Tarjeta buscar(int identificador) {
        return em.find(Tarjeta.class, identificador);
    }

    public void insertar(Tarjeta tarjeta) {
        em.persist(tarjeta);
    }

    public void actualizar(Tarjeta tarjeta) {
        em.merge(tarjeta);
    }

    public void eliminar(Tarjeta tarjeta) {
        em.remove(em.merge(tarjeta));
    }

    public List<Tarjeta> listarTarjetasPorCliente(Cliente cliente) {

        em.merge(cliente);
        return new ArrayList<>(cliente.getTarjetasAsociadas().values());

    }

}
