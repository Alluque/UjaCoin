/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoin.beans;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;

/**
 *
 * @author adria
 */
@Configuration
@EnableWebSecurity
public class ServicioSeguridad extends WebSecurityConfigurerAdapter {

    @Autowired
    DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.inMemoryAuthentication().withUser("alluque").password("{noop}1234qwas").roles("ADMIN");

        auth.jdbcAuthentication().passwordEncoder(NoOpPasswordEncoder.getInstance()).dataSource(dataSource)
                .usersByUsernameQuery("select dni, clave, true from cliente where dni=?")
                .authoritiesByUsernameQuery("select dni, 'USER' from cliente where dni=?")
                .rolePrefix("ROLE_");

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.httpBasic();

        http.authorizeRequests().antMatchers("/ujacoin/clientes").hasRole("ADMIN");
        http.authorizeRequests().antMatchers(HttpMethod.POST, "/ujacoin/clientes/{dni:[a-zA-z0-9]+}/cuentas").hasRole("ADMIN");
        http.authorizeRequests().antMatchers(HttpMethod.DELETE, "/ujacoin/clientes/{dni:[\\d+]}/cuentas/{num:[\\d+]}").hasRole("ADMIN");

        http.authorizeRequests().antMatchers("/ujacoin/clientes/**").hasAnyRole("USER","ADMIN");
        http.authorizeRequests().antMatchers(HttpMethod.POST, "/ujacoin/clientes/**").hasAnyRole("USER","ADMIN");
        http.authorizeRequests().antMatchers(HttpMethod.PUT, "/ujacoin/clientes/**").hasAnyRole("USER","ADMIN");
        http.authorizeRequests().antMatchers(HttpMethod.DELETE, "/ujacoin/clientes/**").hasAnyRole("USER","ADMIN");

        http.authorizeRequests().antMatchers("/ujacoin/enteros/**").permitAll();

        http.authorizeRequests().antMatchers("/**").denyAll();

    }

}
