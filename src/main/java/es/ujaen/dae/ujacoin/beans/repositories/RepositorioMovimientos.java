/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoin.beans.repositories;

import es.ujaen.dae.ujacoin.entidades.Cuenta;
import es.ujaen.dae.ujacoin.entidades.Movimiento;
import es.ujaen.dae.ujacoin.entidades.Tarjeta;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author adria
 */
@Repository
@Transactional
public class RepositorioMovimientos {

    @PersistenceContext
    EntityManager em;

    public Movimiento buscar(String clave) {
        return em.find(Movimiento.class, clave);
    }

    public void insertar(Movimiento movimiento) {
        em.persist(movimiento);
    }

    public void actualizar(Movimiento movimiento) {
        em.merge(movimiento);
    }

    public void eliminar(Movimiento movimiento) {
        em.remove(em.merge(movimiento));
    }

    /**
     * Devolver los movimientos desde y hacia una tarjeta concreta
     *
     * @param tarjeta tarjeta objetivo
     * @return listado de movimientos
     */
    public List<Movimiento> listMovimientos(Tarjeta tarjeta) {

        List<Movimiento> movimientos = em.createQuery("Select m from Movimiento m where m.tarjeta = :tarjeta", Movimiento.class).setParameter("tarjeta", tarjeta.getIdentificador()).getResultList();

        return movimientos;

    }

    /**
     * Devolver los movimientos desde y hacia una tarjeta concreta
     *
     * @param cuenta tarjeta objetivo
     * @return listado de movimientos
     */
    public List<Movimiento> listMovimientos(Cuenta cuenta) {

        List<Movimiento> movimientos = em.createQuery("Select m from Movimiento m where m.cuenta = :cuenta", Movimiento.class).setParameter("cuenta", cuenta.getNumero()).getResultList();

        return movimientos;

    }

}
