/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoin.entidades;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class CuentaTest {
    
    @Test
    public void testNumerosCuenta() {
        String numeroOK = "1234567836";
        String numeroNoOK1 = "1234567899";
        String numeroNoOK2 = "1234599";
        String numeroNoOK3 = "3567833";
        
        Assertions.assertTrue(Cuenta.checkNumeroCuenta(numeroOK));
        Assertions.assertFalse(Cuenta.checkNumeroCuenta(numeroNoOK1));
        Assertions.assertFalse(Cuenta.checkNumeroCuenta(numeroNoOK2));
        Assertions.assertFalse(Cuenta.checkNumeroCuenta(numeroNoOK3));
    }
    
}
