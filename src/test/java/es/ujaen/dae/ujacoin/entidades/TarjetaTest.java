/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoin.entidades;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;


public class TarjetaTest {
    
    @Test
    public void testNumerosTarjeta() {
        
        String numeroOK = "12345678";
        String numeroNoOK1 = "12346678";
        String numeroNoOK2 = "1234578";
        String numeroNoOK3 = "1235678";
        
        Assertions.assertTrue(Tarjeta.checkNumeroTarjeta(numeroOK));
        Assertions.assertFalse(Tarjeta.checkNumeroTarjeta(numeroNoOK1));
        Assertions.assertFalse(Tarjeta.checkNumeroTarjeta(numeroNoOK2));
        Assertions.assertFalse(Tarjeta.checkNumeroTarjeta(numeroNoOK3));
        
    }
    
}
