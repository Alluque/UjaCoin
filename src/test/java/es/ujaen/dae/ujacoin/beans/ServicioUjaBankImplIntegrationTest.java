/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.ujacoin.beans;

import es.ujaen.dae.ujacoin.app.AppUjaBank;
import es.ujaen.dae.ujacoin.entidades.Cliente;
import es.ujaen.dae.ujacoin.entidades.Cuenta;
import java.time.LocalDate;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

/**
 * Test de integración para el controlador del sistema Usa @SpringBootTest para
 * poder hacer el test usando el bean dentro del contenedor IoC de Spring
 */
//@DataJpaTest
//@ContextConfiguration(classes = AppUjaBank.class)
@SpringBootTest(classes = {AppUjaBank.class})
@ActiveProfiles(profiles = "test")
public class ServicioUjaBankImplIntegrationTest {

    @Autowired
    ServicioUjaBankImpl ujaBank;

    /**
     * Test of altaCliente method, of class ServicioUjaBankImpl.
     */
    @Test
    public void testAltaClienteYLoginCliente() {
        ujaBank.altaCliente(new Cliente("12345678A", "Antonio Francisco Postigo", LocalDate.parse("1950-05-13"), "Calle Falsa 123", "654654654", "pureba@uja.es", "1234qwas"));

        Assertions.assertNull(ujaBank.loginCliente("12345678A", "1234qwasd"));
        Assertions.assertNull(ujaBank.loginCliente("123678A", "1234qwas"));
        Assertions.assertNotNull(ujaBank.loginCliente("12345678A", "1234qwas"));

    }

    /**
     * Test of listaTarjetas method, of class ServicioUjaBankImpl.
     */
    @Test
    public void testListaTarjetas() {

    }

    /**
     * Test of listaCuentas method, of class ServicioUjaBankImpl.
     */
    @Test
    public void testListaCuentas() {
        ujaBank.altaCliente(new Cliente("12345679A", "Antonio Francisco Postigo", LocalDate.parse("1950-05-13"), "Calle Falsa 123", "654654654", "pureba@uja.es", "1234qwas"));

        Cliente cliente = ujaBank.loginCliente("12345679A", "1234qwas");

        Assumptions.assumeTrue(cliente != null);

        Assertions.assertEquals(1, ujaBank.listaCuentas(cliente).size());

    }

    /**
     * Test of verCuenta method, of class ServicioUjaBankImpl.
     */
    @Test
    public void testVerCuenta() {

        ujaBank.altaCliente(new Cliente("22345678A", "Antonio Francisco Postigo", LocalDate.parse("1950-05-13"), "Calle Falsa 123", "654654654", "pureba@uja.es", "1234qwas"));
        Cliente cliente = ujaBank.loginCliente("22345678A", "1234qwas");

        Assumptions.assumeTrue(cliente != null);

        ArrayList<Cuenta> cuentas = ujaBank.listaCuentas(cliente);

        cuentas.forEach(cuenta -> {
            Assertions.assertNotNull(ujaBank.verCuenta(cuenta.getNumero()));
        });

    }

    /**
     * Test of ingreso method, of class ServicioUjaBankImpl.
     */
    @Test
    public void testIngreso() {

    }

    /**
     * Test of reintegro method, of class ServicioUjaBankImpl.
     */
    @Test
    public void testReintegro() {

    }

    /**
     * Test of transferencia method, of class ServicioUjaBankImpl.
     */
    @Test
    public void testTransferencia() {

        ujaBank.altaCliente(new Cliente("12345687A", "Antonio Francisco Postigo", LocalDate.parse("1950-05-13"), "Calle Falsa 123", "654654654", "pureba@uja.es", "1234qwas"));
        ujaBank.altaCliente(new Cliente("87654321B", "Antonio Francisco Postigo", LocalDate.parse("1950-02-01"), "Calle Falsa 124", "953010101", "pureba@uja.es", "qwas1234"));

        Cliente clienteA = ujaBank.loginCliente("12345687A", "1234qwas");
        Cliente clienteB = ujaBank.loginCliente("87654321B", "qwas1234");

        Assumptions.assumeTrue(clienteA != null);
        Assumptions.assumeTrue(clienteB != null);

        Cuenta cuentaA = ujaBank.listaCuentas(clienteA).get(0);
        Cuenta cuentaB = ujaBank.listaCuentas(clienteB).get(0);

        ujaBank.asignarSaldoManual(cuentaA, 450);
                

//        ArrayList<Thread> hilos = new ArrayList<>();
//        
//        for (int i = 0; i < 2; i++) {
//            
//            Thread t = new Thread(() -> {
//                ujaBank.transferencia(cuentaA, cuentaB, 125);
//            });
//            hilos.add(t);
//            t.start();
//            
//        }
//        
//        for (Thread hilo : hilos) {
//            try{
//                hilo.join();
//            }catch(InterruptedException e){
//                throw new RuntimeException("Excepción en el join de test");
//            }
//        }
//        
//        Assertions.assertEquals(450, cuentaA.getSaldo() + cuentaB.getSaldo());
        
        ujaBank.transferencia(cuentaA, cuentaB, 125);
        
        clienteA = ujaBank.loginCliente("12345687A", "1234qwas");
        clienteB = ujaBank.loginCliente("87654321B", "qwas1234");
        
        cuentaA = ujaBank.listaCuentas(clienteA).get(0);
        cuentaB = ujaBank.listaCuentas(clienteB).get(0);
        
        Assertions.assertEquals(325, cuentaA.getSaldo());
        Assertions.assertEquals(125, cuentaB.getSaldo());

    }

}
